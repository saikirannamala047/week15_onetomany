package com.gl;

import com.gl.entity.Book;
import com.gl.entity.Page;
import com.gl.repository.BookRepository;
import com.gl.repository.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class App implements CommandLineRunner {
	@Autowired
	BookRepository bookrepo;
	@Autowired
    PageRepository pagerepo;

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	 @Override
	    public void run(String... args) {
		// create a new book
         Book book = new Book("Spring 101", "Sam", "999999");

         // save the book
         bookrepo.save(book);

         // create and save new pages
         pagerepo.save(new Page(1, "Introduction contents", "Introduction", book));
         pagerepo.save(new Page(65, "Spring contents", "Spring 2.1", book));
         pagerepo.save(new Page(95, "IoC contents", "IoC", book));
     }
}
