package com.gl.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.entity.Book;
import com.gl.entity.Page;

public interface PageRepository extends JpaRepository<Page, Long> {
	
	List<Page> findByBook(Book book, Sort sort);

}
